Chart.defaults.global.responsive = true;

var dayPM10 = new Chart(document.getElementById('dayPM10'), {
    type: 'line',
    data: {
        labels: daysenstr,
        datasets: [{
            label: "PM10",
            data: dayPM10data,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Feinstaub PM10 µg/m³"
        }
    }
});

var dayPM2_5 = new Chart(document.getElementById('dayPM2_5'), {
    type: 'line',
    data: {
        labels: daysenstr,
        datasets: [{
            label: "PM2_5",
            data: dayPM2_5data,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Feinstaub PM2_5 µg/m³"
        }
    }
});

var daytemp = new Chart(document.getElementById('daytemp'), {
    type: 'line',
    data: {
        labels: daysenstr,
        datasets: [{
            label: "Temperatur",
            data: daytempdata,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Temperatur °C"
        }
    }
});

var dayhum = new Chart(document.getElementById('dayhum'), {
    type: 'line',
    data: {
        labels: daysenstr,
        datasets: [{
            label: "Luftfeuchte",
            data: dayhumdata,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Luftfeuchte %"
        }
    }
});

var dayabs_press = new Chart(document.getElementById('dayabs_press'), {
    type: 'line',
    data: {
        labels: daysenstr,
        datasets: [{
            label: "Absoluter Luftdruck",
            data: dayabs_pressdata,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Absoluter Luftdruck hPA"
        }
    }
});

var dayred_press = new Chart(document.getElementById('dayred_press'), {
    type: 'line',
    data: {
        labels: daysenstr,
        datasets: [{
            label: "Reduzierter Luftdruck",
            data: dayred_pressdata,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Reduzierter Luftdruck hPA"
        }
    }
});


var monthPM10 = new Chart(document.getElementById('monthPM10'), {
    type: 'line',
    data: {
        labels: monthsenstr,
        datasets: [{
            label: "PM10",
            data: monthPM10data,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Feinstaub PM10 µg/m³"
        }
    }
});

var monthPM2_5 = new Chart(document.getElementById('monthPM2_5'), {
    type: 'line',
    data: {
        labels: monthsenstr,
        datasets: [{
            label: "PM2_5",
            data: monthPM2_5data,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Feinstaub PM2_5 µg/m³"
        }
    }
});

var monthtemp = new Chart(document.getElementById('monthtemp'), {
    type: 'line',
    data: {
        labels: monthsenstr,
        datasets: [{
            label: "Temperatur",
            data: monthtempdata,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Temperatur °C"
        }
    }
});

var monthhum = new Chart(document.getElementById('monthhum'), {
    type: 'line',
    data: {
        labels: monthsenstr,
        datasets: [{
            label: "Luftfeuchte",
            data: monthhumdata,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Luftfeuchte %"
        }
    }
});

var monthabs_press = new Chart(document.getElementById('monthabs_press'), {
    type: 'line',
    data: {
        labels: monthsenstr,
        datasets: [{
            label: "Absoluter Luftdruck",
            data: monthabs_pressdata,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Absoluter Luftdruck hPA"
        }
    }
});

var monthred_press = new Chart(document.getElementById('monthred_press'), {
    type: 'line',
    data: {
        labels: monthsenstr,
        datasets: [{
            label: "Reduzierter Luftdruck",
            data: monthred_pressdata,
            borderColor: "#3e95cd",
            borderWidth: 1,
            pointRadius: 0,
            fill: false
        }]
    },
    options: {
        title: {
            display: true,
            text: "Reduzierter Luftdruck hPA"
        }
    }
});
