<?php

  /* database config */

  define("DB_HOST"     , "localhost");
  define("DB_NAME"     , "databasename");
  define("DB_CHARSET"  , "UTF8");
  define("DB_SERVER"   , "mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=".DB_CHARSET);
  define("DB_USER"     , "username");
  define("DB_PASSWORD" , "password");

  /* config */

  define("SENSOR_DEBUG"    , false);
  define("SENSOR_HEIGHT"   , 75);      //meter over NN
  define("SENSOR_ID"       , 0000000);

