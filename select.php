<?php

/*
 * Select graph data to display
 *
 * @author Maximilian Borm
 * @version 1.3-testing
 */

//TODO: better design 

$min = null;
$max = null;
$avg = null;

require_once 'constArray.php';
require_once 'DB_Functions.php';
$db = new DB_Functions();

//TODO: save last selected date

if(isset($_POST["day"]) && isset($_POST["month"]) && isset($_POST["year"])) {
    $d = ($_POST["day"] === "0") ? NULL : $_POST["day"];
    $m = ($_POST["month"] === "0") ? NULL : $_POST["month"];
    $y = ($_POST["year"] === "0") ? NULL : $_POST["year"];

    $min = $db->getMinData($y, $m, $d);
    $max = $db->getMaxData($y, $m, $d);
    $avg = $db->getAvgData($y, $m, $d);
}

?>
<!doctype html>
<html lang="de">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="copyright" content="mborm" />
        <meta name="robots" content="NOINDEX,NOFOLLOW" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.png" type="image/png" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/OpenSans.css" />
        <link rel="stylesheet" href="css/dark-mode.css" />

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/dark-mode-switch.min.js"></script>

        <title>mborm: Sensor-Daten</title>
    </head>
    <body style="font-family: 'Open Sans'; font-size: 14px">
        <div class="container" style="padding: 30px">
            <div class="pb-2 mt-4 mb-2 border-bottom">
              <h1>LuInAPI</h1>
            </div>
            <div class="wrapper">
                <nav class="nav justify-content-center float-right">
                    <div class="nav-link">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="darkSwitch">
                            <label class="custom-control-label" for="darkSwitch">Dark Mode</label>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <div class="container" style="padding: 30px">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a href="sensor.php" class="nav-link">Sensor-Daten</a></li>
                <li class="nav-item active"><a href="#" class="nav-link active">Zeitauswahl</a></li>
                <li class="nav-item"><a href="graph.php" class="nav-link">Graphen</a></li>
            </ul>
            <br />
            <form class="timepicker" method="POST" action="">
                <div class="row">
                <div class="form-group">
                    <div class="col pull-left">
                        <label for="day">Tag:</label>
                        <select class="form-control" name="day" id="day">
                            <option value="0" selected>0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                    </div>
                    <div class="col pull-left">
                        <label for="month">Monat:</label>
                        <select class="form-control" name="month" id="month">
                            <option value="0" selected>0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <div class="col pull-left">
                        <label for="year">Jahr:</label>
                        <select class="form-control" name="year" id="year">
                            <option value="0" selected>0</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                        </select>
                    </div>
                </div>
                </div>
                <div class="col-md-12">
                    <hr />
                    <div class="form-group">
                        <input type="submit" class="btn btn-secondary" value="Daten anzeigen" />
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#hint">Hinweis zur Benutzung</button>
                    </div>
                </div>
            </form>
            <div class="modal" id="hint" tabindex="-1" role="dialog" aria-labelledby="modalDesc">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalDesc">Hinweise zur Benutzung</h4>
                        </div>
                        <div class="modal-body">
                            <p>Um alle bisherigen Daten anzuzeigen, lassen Sie bitte alle Felder auf "0" stehen. <br /><br />Um nur das Jahr anzuzeigen, lassen Sie die Felder "Tag" und "Monat" auf 0 stehen und stellen Sie das gewünschte Jahr ein. <br /><br />Um nur einen gewünschten Monat anzuzeigen, lassen Sie das Feld "Tag" auf 0 und stellen Sie den gewünschten Monat und das gewünschte Jahr ein.<br /><br />Um einen expliziten Tag anzuzeigen, stellen Sie bitte Tag, Monat und Jahr ein.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
<?php if(is_null($min) || is_null($max) || is_null($avg) || strlen($min[0][0]) < 1) { echo '
                <hr />
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Info</h5>
                    </div>
                    <div class="card-body">
                        <h4>Keine Daten ausgewählt oder für den gewünschten Zeitraum sind keine Daten vorhanden. Bei Problemen, drücken Sie auf "Hinweise zur Benutzung".</h4>
                    </div>
                </div>';
} else {
    echo '      <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Daten für Ihren ausgewählten Zeitraum</h5>
                    </div>
                    <div class="card-body">
                        <h3>Min-Werte</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Sensor-Typ</th>
                                    <th scope="col">Sensor-Wert</th>
                                </tr>
                            </thead>
                            <tbody>';

for($i = 0; $i < count($min[0]); $i++) {
    $value = $min[0][$i];

    if($i == 0 || $i == 1) {
        $color = "black";

        if($value >= 0 && $value <= 25) {
            $color = "green";
        }
        if($value > 25 && $value < 50) {
            $color = "orange";
        }
        if($value >= 50 && $value <= 100) {
            $color = "red";
        }
        if($value > 100) {
            $color = "darkred";
        }

        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td style="color: '.$color.'">'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
    else {
        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td>'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
}

echo '                            </tbody>
                        </table>
                        <br />
                        <h3>Max-Werte</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Sensor-Typ</th>
                                    <th scope="col">Sensor-Wert</th>
                                </tr>
                            </thead>
                            <tbody>';

for($i = 0; $i < count($max[0]); $i++) {
    $value = $max[0][$i];

    if($i == 0 || $i == 1) {
        $color = "black";

        if($value >= 0 && $value <= 25) {
            $color = "green";
        }
        if($value > 25 && $value < 50) {
            $color = "orange";
        }
        if($value >= 50 && $value <= 100) {
            $color = "red";
        }
        if($value > 100) {
            $color = "darkred";
        }

        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td style="color: '.$color.'">'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
    else {
        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td>'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
}

echo '                            </tbody>
                        </table>
                        <br />
                        <h3>Durchschnitts-Werte</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Sensor-Typ</th>
                                    <th scope="col">Sensor-Wert</th>
                                </tr>
                            </thead>
                            <tbody>';

for($i = 0; $i < count($avg[0]); $i++) {
    $value = $avg[0][$i];

    if($i == 0 || $i == 1) {
        $color = "black";

        if($value >= 0 && $value <= 25) {
            $color = "green";
        }
        if($value > 25 && $value < 50) {
            $color = "orange";
        }
        if($value >= 50 && $value <= 100) {
            $color = "red";
        }
        if($value > 100) {
            $color = "darkred";
        }

        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td style="color: '.$color.'">'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
    else {
        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td>'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
}

echo '                          </tbody>
                        </table>
                    </div>
                </div>';
}
?>
            </div>
        </div>
    </body>
</html>
