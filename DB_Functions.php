<?php

/*
 * Database functions
 *
 * @author Maximilian Borm
 * @version 1.4-stable
 */

require_once 'apiconfig.php';

class DB_Functions extends PDO {
    private $db;

    function __construct() {
        if(SENSOR_DEBUG) {
            error_reporting(E_ALL);
            ini_set("error_reporting", -1);
            ini_set("error_log", "/tmp/php-error.log");
        }

        try {
            $this->db = new PDO(DB_SERVER, DB_USER, DB_PASSWORD);
        } catch(PDOException $e) {
            error_log($e->getMessage(), 0);
        }
    }

    function __destruct() {
        $this->db = null;
    }

    public function storeData($pm10, $pm2_5, $temp, $abs_press, $hum, $red_press) {
        $q = $this->db->prepare('INSERT INTO data(timestamp, PM10, PM2_5, temp, hum, abs_press, red_press) VALUES(NOW(), ?, ?, ?, ?, ?, ?)');
        $q->bindValue(1, $pm10, PDO::PARAM_STR);
        $q->bindValue(2, $pm2_5, PDO::PARAM_STR);
        $q->bindValue(3, $temp, PDO::PARAM_STR);
        $q->bindValue(4, $hum, PDO::PARAM_STR);
        $q->bindValue(5, $abs_press, PDO::PARAM_STR);
        $q->bindValue(6, $red_press, PDO::PARAM_STR);
        $q->execute();

        return $q;
    }

    public function getMinData($y = NULL, $m = NULL, $d = NULL) {
        if(is_null($y) && is_null($m) && is_null($d)) { //all time
            $q = $this->db->prepare('SELECT MIN(PM10), MIN(PM2_5), MIN(temp), MIN(hum), MIN(abs_press), MIN(red_press) FROM data;');
        }
        else if(!is_null($y) && is_null($m) && is_null($d)) { //year
            $q = $this->db->prepare('SELECT MIN(PM10), MIN(PM2_5), MIN(temp), MIN(hum), MIN(abs_press), MIN(red_press) FROM data WHERE YEAR(timestamp) = ?;');
            $q->bindValue(1, $y, PDO::PARAM_STR);
        }
        else if(!is_null($y) && !is_null($m) && is_null($d)) { //month and year
            $q = $this->db->prepare('SELECT MIN(PM10), MIN(PM2_5), MIN(temp), MIN(hum), MIN(abs_press), MIN(red_press) FROM data WHERE MONTH(timestamp) = ? AND YEAR(timestamp) = ?;');
            $q->bindValue(1, $m, PDO::PARAM_STR);
            $q->bindValue(2, $y, PDO::PARAM_STR);
        }
        else if(!is_null($y) && !is_null($m) && !is_null($d)) { //explicit day
            $q = $this->db->prepare('SELECT MIN(PM10), MIN(PM2_5), MIN(temp), MIN(hum), MIN(abs_press), MIN(red_press) FROM data WHERE DAY(timestamp) = ? AND MONTH(timestamp) = ? AND YEAR(timestamp) = ?;');
            $q->bindValue(1, $d, PDO::PARAM_STR);
            $q->bindValue(2, $m, PDO::PARAM_STR);
            $q->bindValue(3, $y, PDO::PARAM_STR);
        }
        else {
            return null;
        }

        $q->execute();

        return $q->fetchAll(PDO::FETCH_NUM);
    }

    public function getMaxData($y = NULL, $m = NULL, $d = NULL) {
        if(is_null($y) && is_null($m) && is_null($d)) { //all time
            $q = $this->db->prepare('SELECT MAX(PM10), MAX(PM2_5), MAX(temp), MAX(hum), MAX(abs_press), MAX(red_press) FROM data;');
        }
        else if(!is_null($y) && is_null($m) && is_null($d)) { //year
            $q = $this->db->prepare('SELECT MAX(PM10), MAX(PM2_5), MAX(temp), MAX(hum), MAX(abs_press), MAX(red_press) FROM data WHERE YEAR(timestamp) = ?;');
            $q->bindValue(1, $y, PDO::PARAM_STR);
        }
        else if(!is_null($y) && !is_null($m) && is_null($d)) { //month and year
            $q = $this->db->prepare('SELECT MAX(PM10), MAX(PM2_5), MAX(temp), MAX(hum), MAX(abs_press), MAX(red_press) FROM data WHERE MONTH(timestamp) = ? AND YEAR(timestamp) = ?;');
            $q->bindValue(1, $m, PDO::PARAM_STR);
            $q->bindValue(2, $y, PDO::PARAM_STR);
        }
        else if(!is_null($y) && !is_null($m) && !is_null($d)) { //explicit day
            $q = $this->db->prepare('SELECT MAX(PM10), MAX(PM2_5), MAX(temp), MAX(hum), MAX(abs_press), MAX(red_press) FROM data WHERE DAY(timestamp) = ? AND MONTH(timestamp) = ? AND YEAR(timestamp) = ?;');
            $q->bindValue(1, $d, PDO::PARAM_STR);
            $q->bindValue(2, $m, PDO::PARAM_STR);
            $q->bindValue(3, $y, PDO::PARAM_STR);
        }
        else {
            return null;
        }

        $q->execute();

        return $q->fetchAll(PDO::FETCH_NUM);
    }

    public function getAvgData($y = NULL, $m = NULL, $d = NULL) {
        if(is_null($y) && is_null($m) && is_null($d)) { //all time
            $q = $this->db->prepare('SELECT ROUND(AVG(PM10), 2), ROUND(AVG(PM2_5), 2), ROUND(AVG(temp), 2), ROUND(AVG(hum), 2), ROUND(AVG(abs_press), 2), ROUND(AVG(red_press), 2) FROM data;');
        }
        else if(!is_null($y) && is_null($m) && is_null($d)) { //year
            $q = $this->db->prepare('SELECT ROUND(AVG(PM10), 2), ROUND(AVG(PM2_5), 2), ROUND(AVG(temp), 2), ROUND(AVG(hum), 2), ROUND(AVG(abs_press), 2), ROUND(AVG(red_press), 2) FROM data WHERE YEAR(timestamp) = ?;');
            $q->bindValue(1, $y, PDO::PARAM_STR);
        }
        else if(!is_null($y) && !is_null($m) && is_null($d)) { //month and year
            $q = $this->db->prepare('SELECT ROUND(AVG(PM10), 2), ROUND(AVG(PM2_5), 2), ROUND(AVG(temp), 2), ROUND(AVG(hum), 2), ROUND(AVG(abs_press), 2), ROUND(AVG(red_press), 2) FROM data WHERE MONTH(timestamp) = ? AND YEAR(timestamp) = ?;');
            $q->bindValue(1, $m, PDO::PARAM_STR);
            $q->bindValue(2, $y, PDO::PARAM_STR);
        }
        else if(!is_null($y) && !is_null($m) && !is_null($d)) { //explicit day
            $q = $this->db->prepare('SELECT ROUND(AVG(PM10), 2), ROUND(AVG(PM2_5), 2), ROUND(AVG(temp), 2), ROUND(AVG(hum), 2), ROUND(AVG(abs_press), 2), ROUND(AVG(red_press), 2) FROM data WHERE DAY(timestamp) = ? AND MONTH(timestamp) = ? AND YEAR(timestamp) = ?;');
            $q->bindValue(1, $d, PDO::PARAM_STR);
            $q->bindValue(2, $m, PDO::PARAM_STR);
            $q->bindValue(3, $y, PDO::PARAM_STR);
        }
        else {
            return null;
        }

        $q->execute();

        return $q->fetchAll(PDO::FETCH_NUM);
    }

    public function getGraphData($dmy, $sensor) {
        if($dmy === "m") { //graph for month
            if($sensor === "PM10") {
                $q = $this->db->prepare('SELECT PM10 FROM data WHERE timestamp >= SUBTIME(NOW(), "30 0:0:0")');
            }
            else if($sensor === "PM2_5") {
                $q = $this->db->prepare('SELECT PM2_5 FROM data WHERE timestamp >= SUBTIME(NOW(), "30 0:0:0")');
            }
            else if($sensor === "temp") {
                $q = $this->db->prepare('SELECT temp FROM data WHERE timestamp >= SUBTIME(NOW(), "30 0:0:0")');
            }
            else if($sensor === "hum") {
                $q = $this->db->prepare('SELECT hum FROM data WHERE timestamp >= SUBTIME(NOW(), "30 0:0:0")');
            }
            else if($sensor === "abs_press") {
                $q = $this->db->prepare('SELECT abs_press FROM data WHERE timestamp >= SUBTIME(NOW(), "30 0:0:0")');
            }
            else if($sensor === "red_press") {
                $q = $this->db->prepare('SELECT red_press FROM data WHERE timestamp >= SUBTIME(NOW(), "30 0:0:0")');
            }
            else {
                return null;
            }
        }
        else if($dmy === "d") { //graph for day
            if($sensor === "PM10") {
                $q = $this->db->prepare('SELECT PM10 FROM data WHERE timestamp >= SUBTIME(NOW(), "0 24:0:0")');
            }
            else if($sensor === "PM2_5") {
                $q = $this->db->prepare('SELECT PM2_5 FROM data WHERE timestamp >= SUBTIME(NOW(), "0 24:0:0")');
            }
            else if($sensor === "temp") {
                $q = $this->db->prepare('SELECT temp FROM data WHERE timestamp >= SUBTIME(NOW(), "0 24:0:0")');
            }
            else if($sensor === "hum") {
                $q = $this->db->prepare('SELECT hum FROM data WHERE timestamp >= SUBTIME(NOW(), "0 24:0:0")');
            }
            else if($sensor === "abs_press") {
                $q = $this->db->prepare('SELECT abs_press FROM data WHERE timestamp >= SUBTIME(NOW(), "0 24:0:0")');
            }
            else if($sensor === "red_press") {
                $q = $this->db->prepare('SELECT red_press FROM data WHERE timestamp >= SUBTIME(NOW(), "0 24:0:0")');
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
        
        $q->execute();
        $data = $q->fetchAll(PDO::FETCH_NUM);

        return $data;
    }
}
