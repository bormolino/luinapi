<?php

/*
 * Array constants used in multiple files
 *
 * @author Maximilian Borm
 * @version 1.1-stable
 */

//german translation and measuring unit
$sensorDesc = array("SDS_P1" => array("PM10", "µg/m³"),
                    "SDS_P2" => array("PM2.5", "µg/m³"),
                    "BME280_temperature" => array("Temperatur", "°C"),
                    "BME280_humidity" => array("Luftfeuchte", "%"),
                    "BME280_pressure" => array("abs. Luftdruck", "hPA"),
                    "BME280_pressure_2" => array("red. Luftdruck", "hPA"),
                    "samples" => array("Hauptschleifen-Durchläufe", "Loops"),
                    "min_micro" => array("min. Zeit Schleifendurchlauf", "µs"),
                    "max_micro" => array("max. Zeit Schleifendurchlauf", "µs"),
                    "signal" => array("WLAN-Signalstärke", "dBm"),
                    "temperature" => array("Temperatur", "°C"),
                    "humidity" => array("Luftfeuchte", "%"),
                    "dew_point" => array("Taupunkt", "°C"),
                    "cloud_base" => array("Kondensationsniveau", "m"),
                    "heat_index" => array("Hitzeindex", "°C"),
                    "interval" => array("Intervall", "µs")
                    );

$minSensorDesc = array(array("PM10", "µg/m³"),
                       array("PM2.5", "µg/m³"),
                       array("Temperatur", "°C"),
                       array("Luftfeuchte", "%"),
                       array("abs. Luftdruck", "hPA"),
                       array("red. Luftdruck", "hPA"),
                      );
