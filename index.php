<?php

/*
 * Write sensor data to file - modify json data
 *
 * @author Maximilian Borm
 * @version 1.7-stable
 */

require_once 'apiconfig.php';

$plaindata = file_get_contents("php://input");
$json = json_decode($plaindata, true);

$height = SENSOR_HEIGHT; //meter over NN

if(is_null($json)) {
    error_log("json-parse error - malformed/empty json input or unknown sensor-id", 0);
    exit(1);
}
else {
    $id = $json['esp8266id'];

    //adding sea-level pressure, dew point, cloud base and heat index
    for($i = 0; $i < count($json['sensordatavalues']); $i++) {
        if($json["sensordatavalues"][$i]["value_type"] === "BME280_pressure") {
            $json["sensordatavalues"][$i]["value"] = number_format((floatval($json["sensordatavalues"][$i]["value"]) / 100), 2, '.', '');

            for($j = 0; $j < count($json['sensordatavalues']); $j++) {
                if($json["sensordatavalues"][$j]["value_type"] === "BME280_temperature") {

                    $temp = floatval($json["sensordatavalues"][$j]["value"]);
                    $pabs = floatval($json["sensordatavalues"][$i]["value"]);

                    $pred = number_format(($pabs * pow(1 - 0.0065 * $height / ($temp + 0.0065 * $height + 273.15), -5.257)), 2, '.', '');
                    array_push($json["sensordatavalues"], array("value_type" => "BME280_pressure_2", "value" => $pred));

                    for($p = 0; $p < count($json['sensordatavalues']); $p++) {
                        if($json["sensordatavalues"][$p]["value_type"] === "BME280_humidity") {
                            $hum = floatval($json["sensordatavalues"][$p]["value"]);

                            //dew point and cloud base calculation (NOAA)
                            $d = log($hum * 0.01 * exp((17.67 - $temp / 234.5) * ($temp / (243.5 + $temp))));
                            $dewpoint = number_format(243.5 * $d / (17.67 - $d), 2, '.', '');
                            array_push($json["sensordatavalues"], array("value_type" => "dew_point", "value" => $dewpoint));

                            $cloudbase = number_format(($temp - $d) * 125, 2, '.', '');
                            array_push($json["sensordatavalues"], array("value_type" => "cloud_base", "value" => $cloudbase));

                            //heat index calculation (NOAA)
                            $hi = 1.1 * $temp + 0.026111 * $hum - 3.944444;  //Steadman equation in Celsius

                            if($hi + $temp >= 53.3) {      //use Rothfusz equation above 26.7 *C
                                $h2 = $hum * $hum;
                                $t2 = $temp * $temp;
                                $hi = -8.784695 + 1.61139411 * $temp + 2.338549 * $hum
                                    + -0.146116050 * $temp * $hum + -1.2308094E-02 * $t2 + -1.6424828E-02 * $h2
                                    + 2.211732E-03 * $t2 * $hum + 7.2546E-04 * $temp * $h2 + -3.582E-06 * $t2 * $h2;
                            }

                            array_push($json["sensordatavalues"], array("value_type" => "heat_index", "value" => number_format($hi, 2, '.', '')));
                        }
                    }
                }
            }
        }
    }
    

    //adding unix timestamp and formatted time
    $json["unixtime"] = time();
    $json["formatted_time"] = date("d.m.Y | H:i:s");
    //overwriting file with new data
    $file = json_encode($json);

    if(!is_dir("json"))
      mkdir("json", 0777, true);

    file_put_contents("json/data-".$id.".json", $file);

    require_once 'DB_Functions.php';
    $db = new DB_Functions();
    $varray = array();

    for($i = 0; $i < count($json["sensordatavalues"]); $i++) {
        $valueType = $json["sensordatavalues"][$i]["value_type"];
        $value = $json["sensordatavalues"][$i]["value"];

        if($valueType === "SDS_P1")
            array_push($varray, $value);
        else if($valueType === "SDS_P2")
            array_push($varray, $value);
        else if($valueType === "BME280_temperature")
            array_push($varray, $value);
        else if($valueType === "BME280_humidity")
            array_push($varray, $value);
        else if($valueType === "BME280_pressure")
            array_push($varray, $value);
        else if($valueType === "BME280_pressure_2")
            array_push($varray, $value);
    }

    $r = $db->storeData(...$varray);

    if(!$r) {
        error_log("error storing LuInAPI-data in database", 0);
        exit(1);
    }
}

