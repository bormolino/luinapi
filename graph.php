<?php

/*
 * Display graph data
 *
 * @author Maximilian Borm
 * @version 1.4-testing (BETA)
 */

require_once 'DB_Functions.php';
$db = new DB_Functions();

function generateGraph($dbobject, $dmy, $sensor, $firstrun) {
    if($dmy === "d") {
        $now = date("H:i");
        $senstr = '"'.$now.'",';

        $data = $dbobject->getGraphData($dmy, $sensor);
        $output = 'const day'.$sensor.'data = '.str_replace('"]', '', str_replace('["', '', json_encode($data))).';';
    
        if($firstrun) {
            for($i = 0; $i < count($data) - 3; $i++)
                $senstr .= '"",';

            $senstr .= '"'.$now.'"';
            $output .= 'const daysenstr = ['.$senstr.'];';
        }

        return $output;
    }
    else if($dmy === "m") {
        $senstr = '"-30 Tage",';
        $data = $dbobject->getGraphData($dmy, $sensor);
        $output = 'const month'.$sensor.'data = '.str_replace('"]', '', str_replace('["', '', json_encode($data))).';';

        if($firstrun) {
            for($i = 0; $i < count($data) - 3; $i++)
                $senstr .= '"",';

            $senstr .= '"Heute"';
            $output .= 'const monthsenstr = ['.$senstr.'];';
        }

        return $output;
    }
    else {
        return false;
    }   
}

?>

<!doctype html>
<html lang="de">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="copyright" content="mborm" />
        <meta name="robots" content="NOINDEX,NOFOLLOW" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.png" type="image/png" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/OpenSans.css" />
        <link rel="stylesheet" href="css/dark-mode.css" />

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/dark-mode-switch.min.js"></script>
        <script src="js/Chart.bundle.min.js"></script>

        <script type="text/javascript">
<?php

    echo '//generated data for day graphs';
    echo "\n";

    echo generateGraph($db, "d", "PM10", true);
    echo generateGraph($db, "d", "PM2_5", false);
    echo generateGraph($db, "d", "temp", false);
    echo generateGraph($db, "d", "hum", false);
    echo generateGraph($db, "d", "abs_press", false);
    echo generateGraph($db, "d", "red_press", false);

    echo generateGraph($db, "m", "PM10", true);
    echo generateGraph($db, "m", "PM2_5", false);
    echo generateGraph($db, "m", "temp", false);
    echo generateGraph($db, "m", "hum", false);
    echo generateGraph($db, "m", "abs_press", false);
    echo generateGraph($db, "m", "red_press", false);


//TODO: maybe calc avg values for faster loading

?>
        </script>
        <title>mborm: Sensor-Daten</title>
    </head>
    <body style="font-family: 'Open Sans'; font-size: 14px">
        <div class="container" style="padding: 30px">
            <div class="pb-2 mt-4 mb-2 border-bottom">
              <h1>LuInAPI</h1>
            </div>
            <div class="wrapper">
                <nav class="nav justify-content-center float-right">
                    <div class="nav-link">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="darkSwitch">
                            <label class="custom-control-label" for="darkSwitch">Dark Mode</label>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <div class="container" style="padding: 30px">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a href="sensor.php" class="nav-link">Sensor-Daten</a></li>
                <li class="nav-item"><a href="select.php" class="nav-link">Zeitauswahl</a></li>
                <li class="nav-item active"><a href="#" class="nav-link active">Graphen</a></li>
            </ul>
            <br />
            <div class="col-12">
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Feinstaub PM10 Tagesgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="dayPM10" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Feinstaub PM2.5 Tagesgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="dayPM2_5" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Temperatur Tagesgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="daytemp" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Luftfeuchte Tagesgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="dayhum" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Absoluter Luftdruck Tagesgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="dayabs_press" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Reduzierter Luftdruck Tagesgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="dayred_press" width="100%" height="50%"></canvas>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Feinstaub PM10 Monatsgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="monthPM10" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Feinstaub PM2.5 Monatsgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="monthPM2_5" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Temperatur Monatsgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="monthtemp" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Luftfeuchte Monatsgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="monthhum" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Absoluter Luftdruck Monatsgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="monthabs_press" width="100%" height="50%"></canvas>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Reduzierter Luftdruck Monatsgraph</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="monthred_press" width="100%" height="50%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    <script src="js/chart.js"></script>
    </body>
</html>
