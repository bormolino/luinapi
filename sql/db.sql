CREATE TABLE `data` (
  `timestamp` datetime NOT NULL,
  `PM10` double NOT NULL,
  `PM2_5` double NOT NULL,
  `temp` double NOT NULL,
  `hum` double NOT NULL,
  `abs_press` double NOT NULL,
  `red_press` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

ALTER TABLE `data`
  ADD PRIMARY KEY (`timestamp`);

