<?php

/*
 * Display sensor data
 *
 * @author Maximilian Borm
 * @version 2.3-stable
 */

require_once 'apiconfig.php';
$id = SENSOR_ID;

$data = file_get_contents("json/data-".$id.".json");
$json = json_decode($data, true);

if(is_null($json)) {
    echo "Server error while parsing json file";
    error_log("json-parse error - malformed or empty json input", 0);
    exit(1);
}

require_once 'constArray.php';
require_once 'DB_Functions.php';
$db = new DB_Functions();

//get date of month
$y = date('Y');
$m = date('m');

$min = $db->getMinData($y, $m);
$max = $db->getMaxData($y, $m);
$avg = $db->getAvgData($y, $m);


?>
<!doctype html>
<html lang="de">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="copyright" content="mborm" />
        <meta name="robots" content="NOINDEX,NOFOLLOW" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.png" type="image/png" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/OpenSans.css" />
        <link rel="stylesheet" href="css/dark-mode.css" />

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/dark-mode-switch.min.js"></script>

        <title>mborm: Sensor-Daten</title>
    </head>
    <body style="font-family: 'Open Sans'; font-size: 14px">
        <div class="container" style="padding: 30px">
            <div class="pb-2 mt-4 mb-2 border-bottom">
              <h1>LuInAPI</h1>
            </div>
            <div class="wrapper">
                <nav class="nav justify-content-center float-right">
                    <div class="nav-link">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="darkSwitch">
                            <label class="custom-control-label" for="darkSwitch">Dark Mode</label>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <div class="container" style="padding: 30px">
            <ul class="nav nav-tabs">
                <li class="nav-item active"><a href="#" class="nav-link active">Sensor-Daten</a></li>
                <li class="nav-item"><a href="select.php" class="nav-link">Zeitauswahl</a></li>
                <li class="nav-item"><a href="graph.php" class="nav-link">Graphen</a></li>
            </ul>
            <br />
            <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Messdatum: <?php echo $json["formatted_time"]; ?></h5>
                    </div>
                    <div class="card-body">
                            <?php
                              for($i = 0; $i < count($json['sensordatavalues']); $i++) {
                                  $valueType = $json["sensordatavalues"][$i]["value_type"];
                                  $value = round($json["sensordatavalues"][$i]["value"]);

                                  if($valueType === "BME280_pressure_2") {
                                    $color = "black"; $weather = "";

                                    if($value >= 975 && $value <= 995) {
                                      $color = "red"; $weather = "Sturm/Unwetter";
                                    }
                                    else if($value >= 996 && $value <= 1030) {
                                      $color = "orange"; $weather = "Veränderlich";
                                    }
                                    else if($value >= 1031 && $value <= 1060) {
                                      $color = "green"; $weather = "Schönes Wetter";
                                    }
                                    else {
                                      break;
                                    }

                                    echo '<div class="forecast alert bg-light" role="alert">Voraussichtliches Wetter: <span style="color: '.$color.'">'.$weather.'</span></div>';
                                  }
                              } ?>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Sensor-Typ</th>
                                    <th scope="col">Sensor-Wert</th>
                                </tr>
                            </thead>
                            <tbody>

<?php
for($i = 0; $i < count($json['sensordatavalues']); $i++) {
    $valueType = $json["sensordatavalues"][$i]["value_type"];
    $value = $json["sensordatavalues"][$i]["value"];

    if($valueType === "SDS_P1" || $valueType === "SDS_P2") {
        $color = "black";

        if($value >= 0 && $value <= 25) {
            $color = "green";
        }
        if($value > 25 && $value < 50) {
            $color = "orange";
        }
        if($value >= 50 && $value <= 100) {
            $color = "red";
        }
        if($value > 100) {
            $color = "darkred";
        }

        echo '<tr><th scope="row">'.$sensorDesc[$valueType][0].'</th><td style="color: '.$color.'">'.htmlspecialchars($value)." ".$sensorDesc[$valueType][1].'</td></tr>';
    }
    else {
        echo '<tr><th scope="row">'.$sensorDesc[$valueType][0].'</th><td>'.htmlspecialchars($value)." ".$sensorDesc[$valueType][1].'</td></tr>';
    }
}
?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class ="col">
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="card-title pull-left">Min-, Max- und Durchschnittswerte dieses Monats</h5>
                    </div>
                    <div class="card-body">
                        <h3>Min-Werte</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Sensor-Typ</th>
                                    <th scope="col">Sensor-Wert</th>
                                </tr>
                            </thead>
                            <tbody>

<?php

for($i = 0; $i < count($min[0]); $i++) {
    $value = $min[0][$i];

    if($i == 0 || $i == 1) {
        $color = "black";

        if($value >= 0 && $value <= 25) {
            $color = "green";
        }
        if($value > 25 && $value < 50) {
            $color = "orange";
        }
        if($value >= 50 && $value <= 100) {
            $color = "red";
        }
        if($value > 100) {
            $color = "darkred";
        }

        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td style="color: '.$color.'">'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
    else {
        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td>'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
}
?>
                            </tbody>
                        </table>
                        <br />
                        <h3>Max-Werte</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Sensor-Typ</th>
                                    <th scope="col">Sensor-Wert</th>
                                </tr>
                            </thead>
                            <tbody>

<?php

for($i = 0; $i < count($max[0]); $i++) {
    $value = $max[0][$i];

    if($i == 0 || $i == 1) {
        $color = "black";

        if($value >= 0 && $value <= 25) {
            $color = "green";
        }
        if($value > 25 && $value < 50) {
            $color = "orange";
        }
        if($value >= 50 && $value <= 100) {
            $color = "red";
        }
        if($value > 100) {
            $color = "darkred";
        }

        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td style="color: '.$color.'">'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
    else {
        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td>'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
}
?>

                            </tbody>
                        </table>
                        <br />
                        <h3>Durchschnitts-Werte</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Sensor-Typ</th>
                                    <th scope="col">Sensor-Wert</th>
                                </tr>
                            </thead>
                            <tbody>

<?php

for($i = 0; $i < count($avg[0]); $i++) {
    $value = $avg[0][$i];

    if($i == 0 || $i == 1) {
        $color = "black";

        if($value >= 0 && $value <= 25) {
            $color = "green";
        }
        if($value > 25 && $value < 50) {
            $color = "orange";
        }
        if($value >= 50 && $value <= 100) {
            $color = "red";
        }
        if($value > 100) {
            $color = "darkred";
        }

        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td style="color: '.$color.'">'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
    else {
        echo '<tr><th scope="row">'.$minSensorDesc[$i][0].'</th><td>'.htmlspecialchars($value)." ".$minSensorDesc[$i][1].'</td></tr>';
    }
}
?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </body>
</html>
