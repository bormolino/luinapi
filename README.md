# LuInAPI -  Eigene luftdaten.info API zur Darstellung der eigenen Messwerte

Die API unterstützt aktuell die Sensoren:
* SDS011
* BME280

### Config Optionen
* SENSOR_DEBUG \[boolean value] zum Loggen der Fehlermeldungen nach /tmp/php-error.log und zur direkten Anzeige der Fehler auf der Webseite. Nicht im Produktivbetrieb verwenden!
* SENSOR_HEIGHT \[numeric value] Höhe über NN des Sensors zur korrekten Berechnung der angezeigten Werte auf sensors.php.
* SENSOR_ID \[numeric value] ID des Feinstaubsensors.

In der apiconfig.php werden auch die Zugangsdaten für die Datenbank angegeben. Im Ordner sql findet sich eine Vorlage der Tabelle. Getestet ist die Software aktuell nur auf MySQL.

Damit die API die Werte empfangen kann muss im Feinstaubsensor unter Konfiguration > APIs der Haken bei "An eigene API senden" aktiviert werden. Dort gibt man dann IP-Adresse/Domain und Pfad an. (Pfad zur index.php).

Der Code ist nicht unbedingt der schönste und effizienteste. Das liegt daran, dass es erst nur eine lokale Spielerei war. Aber aufgrund mehrerer Nachfragen habe ich den Code hier leicht überarbeitet veröffentlicht.

Sollten Sie einen Fehler finden, erstellen Sie bitte hier ein Ticket.

## Autor
* **Maximilian Borm** - *Developer* - [@bormolino](https://gitlab.com/bormolino)
